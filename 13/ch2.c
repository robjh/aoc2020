#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <pthread.h>

int extract_numbers(const char * input, int num[]) {
	int len = 0;
	do {
		num[len] = (input[0] == 'x' ? 1 : atoi(input));
		++len;
	} while((input = strchr(input,',')) && ++input);
	return len;
}

int test_timestamp(long long int ts, int num[], int num_c) {
	for (int i = 0 ; i < num_c ; ++i) {
		if ((ts+i) % num[i] != 0) return 0;
	}
	return 1;
}

#define THREADCOUNT 32
typedef struct ThreadCtx {
	pthread_t handle;
	int offset;
} ThreadCtx;
typedef void * (*thread_func)(void *x_void_ptr);

//void *thread_main(ThreadCtx * ctx) {
//		long long int ts = ctx->offset;
//		while(test_timestamp(ts * biggest - biggest_i, num, num_c) == 0);

//	return NULL;
//}

int main(const int argc, const char * argv[]) {
	const size_t buffer_l = 1000;
	char buffer[buffer_l];
	int num[100] = { 0 };

	ThreadCtx tctx[THREADCOUNT];

	// discard first line.
	fgets(buffer, buffer_l, stdin);

	while (fgets(buffer, buffer_l, stdin)) {
		if (buffer[0] == '\n') continue;
		int num_c = extract_numbers(buffer, num);

		int biggest = 0;
		int biggest_i = 0;
		for (int i = 0 ; i < num_c ; ++i) {
			if (num[i] > biggest) {
				biggest = num[i];
				biggest_i = i;
			}
		}

		// for my input, this will eventually find 487905974205117
		long long int ts = 0;
		long long int t2 = ts;
		printf("%lld %lld\n\n", ts, ts%biggest);
		while(test_timestamp(++ts * biggest - biggest_i, num, num_c) == 0) {
			if (t2 < ts) {
				printf("%lld\r", ts * biggest);
				t2 = ts + 2000000;
			}
		}

		printf("\n\n\n%d  %lld\n", biggest, ts * biggest - biggest_i);
	}

	return 0;
}
