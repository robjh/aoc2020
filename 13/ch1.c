#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

int extract_numbers(const char * input, int num[]) {
	int len = 0;
	do {
//		num[len++] = input[0] == 'x' ? -1 : atoi(input);
		if (input[0] != 'x') num[len++] = atoi(input);
	} while((input = strchr(input,',')) && ++input);
	return len;
}

int main(const int argc, const char * argv[]) {
	const size_t buffer_l = 1000;
	char buffer[buffer_l];
	int num[100] = { 0 };
	int earliest;

	fgets(buffer, buffer_l, stdin);
	earliest = atoi(buffer);

	fgets(buffer, buffer_l, stdin);
	int num_c = extract_numbers(buffer, num);

	int smallest_wait = 0;
	int smallest_wait_value = INT_MAX;

	for (int i = 0 ; i < num_c ; ++i) {
		int wait = (earliest/num[i]) * num[i] + num[i] - earliest;
		if (wait < smallest_wait_value) {
			smallest_wait_value = wait;
			smallest_wait = i;
		}
	}
	printf("next bus: %d, minutes: %d, multiplied: %d\n", num[smallest_wait], smallest_wait_value, num[smallest_wait] * smallest_wait_value);

	return 0;
}
