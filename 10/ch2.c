#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

typedef uint32_t jolt_t;
#define JOLT_S "%d"

typedef int (*Comparitor_p)(const void *, const void *);
int comparitor(const jolt_t* a, const jolt_t* b) {
	return *a > *b;
}

typedef struct Consider {
	jolt_t number;
	uint64_t viable_paths; // trillions...
} Consider;

// Look at the next 3 numbers.
// If we can leap there from here, Add their path totals to ours.
uint64_t count_paths(jolt_t num[], size_t num_c) {
	const int consider_c = 4;
	Consider consider[consider_c];

	memset(consider, 0, consider_c * sizeof(Consider));

	// prime the datastructure
	consider[0].number = num[--num_c];
	consider[0].viable_paths = 1;

	--num_c;
	do {
		memcpy(consider+1, consider, sizeof(Consider) * (consider_c - 1));
		consider[0].number = num[num_c];
		consider[0].viable_paths = 0;
		for (int i = 1 ; i < consider_c ; ++i) {
			if (consider[i].number <= (num[num_c] + 3)) {
				consider[0].viable_paths += consider[i].viable_paths;
			}
		}
	} while(num_c--);
	return consider[0].viable_paths;
}

int main(const int argc, const char * argv[]) {
	const size_t buffer_l = 16;
	char buffer[buffer_l];
	const size_t num_l = 1000;
	size_t num_c = 0;
	jolt_t num[num_l];
	int diffs[3] = { 0,0,0 };

	memset(num, 0, num_c * sizeof(jolt_t));

	while (fgets(buffer, buffer_l, stdin)) {
		if(buffer[0] == '\n') continue; // empty line
		num[num_c++] = atoll(buffer);
	}

	num[num_c++] = 0;

	qsort(num, num_c, sizeof(jolt_t), (Comparitor_p)&comparitor);

	num[num_c] = num[num_c-1] + 3;
	++num_c;

	printf("total possible paths: %lld\n", count_paths(num, num_c));

	return 0;
}
