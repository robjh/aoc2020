#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef int (*Comparitor_p)(const void *, const void *);
int comparitor(const uint64_t* a, const uint64_t* b) {
	return *a > *b;
}

int main(const int argc, const char * argv[]) {
	const size_t buffer_l = 16;
	char buffer[buffer_l];
	const size_t num_l = 1000;
	size_t num_c = 0;
	uint64_t num[num_l];
	int diffs[3] = { 1,0,1 }; // implicit input device (0 jolt power socket) and output device

	while (fgets(buffer, buffer_l, stdin)) {
		if(buffer[0] == '\n') continue; // empty line
		num[num_c++] = atoll(buffer);
	}

	qsort(num, num_c, sizeof(uint64_t), (Comparitor_p)&comparitor);

	for (int i = 1 ; i < num_c ; ++i) {
		++diffs[num[i] - num[i-1] - 1];
	}
	printf(
		"(1, 2, 3): (%d, %d, %d)\n"
		"(1 * 3)  : (%d)\n",
		diffs[0], diffs[1], diffs[2],
		diffs[0] * diffs[2]
	);

	return 0;
}
