#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "magic_numbers.h"

typedef struct PassportCtx {
	int fields_present;
} PassportCtx;

int validate(const PassportCtx* ctx) {
	// all flags required but CID
	const int fields_required = ((1<<FLAG_COUNT) -1) & ~(1 << FLAG_CID);
	return (fields_required & ctx->fields_present) == fields_required;
}

int main(void) {
	const size_t buffer_l = 128;
	char buffer[buffer_l];

	PassportCtx passport = { 0 };
	int valid_passports = 0;

	while (fgets(buffer, buffer_l, stdin)) {
		if (buffer[0] == '\n') {
			if (validate(&passport)) {
				++valid_passports;
			}
			passport = (const PassportCtx){ 0 };
			continue;
		}
		for (char * buffer_p = buffer ; buffer_p ; (buffer_p = strchr(buffer_p, ' ')) && ++buffer_p) {
			passport.fields_present |= magictoflag(buffer_p);
			// the challenge ignores the data? no validation at all!
		}
	}

	printf("%d\n", valid_passports);
	return 0;
}
