#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "magic_numbers.h"

enum Distance_Units {
	DIST_NONE,
	CM,
	IN
};

enum Eye_Colour {
	EYE_NONE,
	AMB,
	BLU,
	BRN,
	GRY,
	GRN,
	HZL,
	OTH
};

typedef struct PassportCtx {
	int fields_present;
	int birth_year;
	int issue_year;
	int exper_year;
	int height_value;
	enum Distance_Units height_unit;
	char hair_colour[8];
	enum Eye_Colour eye_colour;
	char passport_id[10];
} PassportCtx;

void print_passport(const PassportCtx* ctx) {
	printf(
		"birth year: %d\n"
		"issue year: %d\n"
		"exper year: %d\n"
		"height: %d%s\n"
		"hair colour: %s\n"
		"eye colour: %d\n"
		"passport id: %s\n"
		,
		ctx->birth_year,
		ctx->issue_year,
		ctx->exper_year,
		ctx->height_value, ctx->height_unit == CM ? "cm" : ctx->height_unit == IN ? "in" : "??",
		ctx->hair_colour,
		ctx->eye_colour,
		ctx->passport_id
	);
}

void extract_next_property(const char * str, PassportCtx* ctx) {
	// extract the details to the context.
	switch(((int*)str)[0]) {
		case MAGIC_PID:
			if (str[13] != ' ' && str[13] != '\n') break;
			memcpy(ctx->passport_id, str+4, 9);
			ctx->hair_colour[9] = '\0';
			break;
		case MAGIC_ECL:
			switch(str[4]*str[5]*str[6]) {
				case 'a'*'m'*'b': ctx->eye_colour = AMB; break;
				case 'b'*'l'*'u': ctx->eye_colour = BLU; break;
				case 'b'*'r'*'n': ctx->eye_colour = BRN; break;
				case 'g'*'r'*'y': ctx->eye_colour = GRY; break;
				case 'g'*'r'*'n': ctx->eye_colour = GRN; break;
				case 'h'*'z'*'l': ctx->eye_colour = HZL; break;
				case 'o'*'t'*'h': ctx->eye_colour = OTH; break;
				default: break;
			}
			break;
		case MAGIC_HCL:
			if (str[11] != ' ' && str[11] != '\n') break;
			memcpy(ctx->hair_colour, str+4, 7);
			ctx->hair_colour[7] = '\0';
			break;
		case MAGIC_HGT:
			ctx->height_value = atoi(str+4);
			for (str+=4 ; *str >= '0' && *str <= '9' ; ++str);
			if (memcmp("in", str, 2) == 0) ctx->height_unit = IN;
			if (memcmp("cm", str, 2) == 0) ctx->height_unit = CM;
			break;
		case MAGIC_EYR:
			ctx->exper_year = atoi(str+4);
			break;
		case MAGIC_IYR:
			ctx->issue_year = atoi(str+4);
			break;
		case MAGIC_BYR:
			ctx->birth_year = atoi(str+4);
			break;
		default: break;
	}
}

int validate(const PassportCtx* ctx) {
	// all flags required but CID
	const int fields_required = ((1<<FLAG_COUNT) -1) & ~(1 << FLAG_CID);
	if (!((fields_required & ctx->fields_present) == fields_required)) {
		return 0;
	}

	if (ctx->birth_year < 1920 || ctx->birth_year > 2002) return 0;
	if (ctx->issue_year < 2010 || ctx->issue_year > 2020) return 0;
	if (ctx->exper_year < 2020 || ctx->exper_year > 2030) return 0;
	if (!ctx->height_unit) return 0;
	if (ctx->height_unit == CM && (ctx->height_value < 150 || ctx->height_value > 193)) return 0;
	if (ctx->height_unit == IN && (ctx->height_value < 59 || ctx->height_value > 76)) return 0;
	if (ctx->hair_colour[0] != '#') return 0;
	for (int i = 1 ; i < 7 ; ++i) if (!((ctx->hair_colour[i] >= '0' && ctx->hair_colour[i] <= '9')
	                                 || (ctx->hair_colour[i] >= 'a' && ctx->hair_colour[i] <= 'f'))) return 0;
	if (!ctx->eye_colour) return 0;
	for (int i = 0 ; i < 9 ; ++i) if (!(ctx->passport_id[i] >= '0' && ctx->passport_id[i] <= '9')) return 0;

	return 1;
}

int main(void) {
	const size_t buffer_l = 128;
	char buffer[buffer_l];

	PassportCtx passport = { 0 };
	int valid_passports = 0;

	while (fgets(buffer, buffer_l, stdin)) {
		if (buffer[0] == '\n') {
			if (validate(&passport)) {
				++valid_passports;
			}
			passport = (const PassportCtx){ 0 };
			continue;
		}
		for (char * buffer_p = buffer ; buffer_p ; (buffer_p = strchr(buffer_p, ' ')) && ++buffer_p) {
			passport.fields_present |= magictoflag(buffer_p);
			extract_next_property(buffer_p, &passport);
		}
	}

	printf("%d\n", valid_passports);
	return 0;
}
