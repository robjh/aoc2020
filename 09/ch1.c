#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

size_t find_weakness(uint64_t num[], size_t size, int preamble) {
	for (size_t x = preamble ; x < size ; ++x) {
		int match = 0;
		for (size_t y = x - preamble ; y < x ; ++y) {
			for (size_t z = y+1 ; z < x ; ++z) {
				if (num[y] + num[z] == num[x]) {
					match = 1;
				}
			}
		}
		if (!match) return x;
	}
	return 0;
}

int main(const int argc, const char * argv[]) {
	const size_t buffer_l = 16;
	char buffer[buffer_l];
	const size_t nums_l = 1000;
	size_t nums_c = 0;
	uint64_t nums[nums_l];

	int preamble = 0;

	if (argc >= 2) {
		preamble = atoi(argv[1]);
	}
	if (!preamble) {
		preamble = 25;
	}

	while (fgets(buffer, buffer_l, stdin)) {
		if(buffer[0] == '\n') continue; // empty line
		nums[nums_c++] = atoll(buffer);
	}

	size_t weakness = find_weakness(nums, nums_c, preamble);
	printf("weakness at (%d): %lld\n", weakness, nums[weakness]);

	return 0;
}
