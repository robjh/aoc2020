#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

size_t find_weakness(uint64_t num[], size_t size, int preamble) {
	for (size_t x = preamble ; x < size ; ++x) {
		int match = 0;
		for (size_t y = x - preamble ; y < x ; ++y) {
			for (size_t z = y+1 ; z < x ; ++z) {
				if (num[y] + num[z] == num[x]) {
					match = 1;
				}
			}
		}
		if (!match) return x;
	}
	return 0;
}

typedef struct Range {
	size_t index;
	size_t count;
} Range;

Range find_contiguous_set(uint64_t num[], uint64_t target) {
	size_t index;
	uint64_t sum = 0;
	printf("target: %lld\n", target);
	for (index = 0 ; num[index] < target ; ++index) {
		sum = 0;
		for (size_t j = index; num[j] < target ; ++j) {
			sum += num[j];
			if (sum == target) {
				return (Range){index, j - index + 1};
			} else if (sum > target) {
				break;
			}
		}
	}
	return (const Range){0};
}

uint64_t largest_in_range(uint64_t num[], const Range * range) {
	uint64_t largest = num[range->index];
	for (size_t i = 1 ; i < range->count ; ++i) {
		if (num[range->index + i] > largest) largest = num[range->index + i];
	}
	return largest;
}

uint64_t smallest_in_range(uint64_t num[], const Range * range) {
	uint64_t smallest = num[range->index];
	for (size_t i = 1 ; i < range->count ; ++i) {
		if (num[range->index + i] < smallest) smallest = num[range->index + i];
	}
	return smallest;
}

int main(const int argc, const char * argv[]) {
	const size_t buffer_l = 16;
	char buffer[buffer_l];
	const size_t nums_l = 1000;
	size_t nums_c = 0;
	uint64_t nums[nums_l];

	int preamble = 0;

	if (argc >= 2) {
		preamble = atoi(argv[1]);
	}
	if (!preamble) {
		preamble = 25;
	}

	while (fgets(buffer, buffer_l, stdin)) {
		if(buffer[0] == '\n') continue; // empty line
		nums[nums_c++] = atoll(buffer);
	}

	size_t weakness = find_weakness(nums, nums_c, preamble);

	Range cnt = find_contiguous_set(nums, nums[weakness]);
	if (cnt.index) {
		printf("found continguous range at: %d - %d\n", cnt.index, cnt.count);
		printf("%lld\n", smallest_in_range(nums, &cnt) + largest_in_range(nums, &cnt));
	}

	return 0;
}
