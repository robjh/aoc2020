#include <stdio.h>

int bitcount(unsigned int num) {
	int ret = 0;
	for (int i = 0 ; i < 26 ; ++i) {
		if (num & (1 << i)) ++ret;
	}
	return ret;
}

int main(void) {
	const size_t buffer_l = 128;
	char buffer[buffer_l];

	unsigned int sum = 0;
	const int firstrow = 1 << 26;
	unsigned int q_reg_common = firstrow;
	unsigned int q_reg;

	while (fgets(buffer, buffer_l, stdin)) {
		if (buffer[0] == '\n') {
			sum += bitcount(q_reg_common);
			q_reg_common = firstrow;
			continue;
		}

		q_reg = 0;
		for (int i = 0 ; buffer[i] != '\n' ; ++i) {
			q_reg |= 1 << (buffer[i] - 'a');
		}

		if (q_reg_common == firstrow) {
			q_reg_common = q_reg;
		} else {
			q_reg_common &= q_reg;
		}
	}

	printf("%d\n", sum);

	return 0;
}
