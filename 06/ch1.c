#include <stdio.h>

int bitcount(unsigned int num) {
	int ret = 0;
	for (int i = 0 ; i < 32 ; ++i) {
		if (num & (1 << i)) ++ret;
	}
	return ret;
}

int main(void) {
	const size_t buffer_l = 128;
	char buffer[buffer_l];

	unsigned int sum = 0;
	unsigned int q_reg = 0;

	while (fgets(buffer, buffer_l, stdin)) {
		if (buffer[0] == '\n') {
			sum += bitcount(q_reg);
			q_reg = 0;
		}

		for (int i = 0 ; buffer[i] != '\n' ; ++i) {
			q_reg |= 1 << (buffer[i] - 'a');
		}

	}

	printf("%d\n", sum);

	return 0;
}
