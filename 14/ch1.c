#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <map>
#include "magic_numbers.h"

typedef struct Mask {
	uint64_t high = 0;
	uint64_t low  = 0;
	uint64_t full = 0;
} Mask;

void extract_mask(const char * str, Mask * mask) {
	mask->high = mask->low = 0ULL;
	const int bit_c = 36;
	for (uint64_t i = 0 ; i < bit_c ; ++i) {
		switch (str[i*-1+bit_c]) {
			case '0':
				mask->low  |= 0x1ULL << i;
				break;
			case '1':
				mask->high |= 0x1ULL << i;
				break;
		}
	}
}

void print_mask(const Mask * mask) {
	const int bit_c = 36;
	for (int i = bit_c; i >= 0 ; --i) {
		printf("%c", mask->high & (1ULL << i) ? '1' : mask->low & (1ULL << i) ? '0' : 'X');
	}
	printf("\n");
}

typedef struct Pair {
	int index;
	uint64_t value;
} Pair;

void extract_pair(const char * str, Pair * pair) {
	str += 4;
	pair->index = atoi(str);
	str = strchr(str, '=') + 2;
	pair->value = atoll(str);
}

void apply_mask(const Mask * mask, uint64_t * value) {
	*value |= mask->high;
	*value &= ~mask->low;
	*value &= mask->full;
}

int main(const int argc, const char * argv[]) {
	const size_t buffer_l = 50;
	char buffer[buffer_l];
	Mask mask;
	mask.full = 0x0000000FFFFFFFFF;
	Pair pair;

	std::map<int, uint64_t> mem;

	while (fgets(buffer, buffer_l, stdin)) {
		if (buffer[0] == '\n') continue;
		switch(((int*)buffer)[0]) {
			case MAGIC_MAS:
				extract_mask(buffer+6, &mask);
				print_mask(&mask);
				break;
			case MAGIC_MEM:
				extract_pair(buffer, &pair);
				printf("mem[%d] = %lld\n", pair.index, pair.value);

				apply_mask(&mask, &pair.value);
				mem[pair.index] = pair.value;
				break;
		}
	}

	int64_t total = 0;
	for (
		std::map<int, uint64_t>::iterator it = mem.begin();
		it != mem.end();
		++it
	) {
//		printf("mem[%d] = %lld\n", it->first, it->second);
		total += it->second;
	}

	printf("%lld\n", total);

	return 0;
}
