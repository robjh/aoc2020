#include <stdio.h>

typedef struct FindseatCtx {
	int front;
	int back;
	int left;
	int right;
} FindseatCtx;

void findseat(const char * input, FindseatCtx * ctx) {
	switch (input[0]) {
		case 'F': ctx->back  -= (ctx->back - ctx->front) / 2 + 1; break;
		case 'B': ctx->front += (ctx->back - ctx->front) / 2 + 1; break;
		case 'L': ctx->right -= (ctx->right - ctx->left) / 2 + 1; break;
		case 'R': ctx->left  += (ctx->right - ctx->left) / 2 + 1; break;
	}
//	printf("%c: %d %d %d %d\n", input[0], ctx->front, ctx->back, ctx->left, ctx->right);
	if (input[1] != '\0') findseat(input + 1, ctx);
}

int main(void) {
	const size_t buffer_l = 128;
	char buffer[buffer_l];
	int highest = 0;
	int current;

	FindseatCtx ctx;

	while (fgets(buffer, buffer_l, stdin)) {
		ctx = (const FindseatCtx){ 0, 127, 0, 7 };
		buffer[10] = '\0';
		findseat(buffer, &ctx);
		current = ctx.front * 8 + ctx.left;
		printf("%s %d %d %d\n", buffer, ctx.front, ctx.left, current);
		if (current > highest) highest = current;
	}
	printf("highest: %d\n", highest);

	return 0;
}
