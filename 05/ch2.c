#include <stdio.h>

typedef struct FindseatCtx {
	int front;
	int back;
	int left;
	int right;
} FindseatCtx;

void findseat(const char * input, FindseatCtx * ctx) {
	switch (input[0]) {
		case 'F': ctx->back  -= (ctx->back - ctx->front) / 2 + 1; break;
		case 'B': ctx->front += (ctx->back - ctx->front) / 2 + 1; break;
		case 'L': ctx->right -= (ctx->right - ctx->left) / 2 + 1; break;
		case 'R': ctx->left  += (ctx->right - ctx->left) / 2 + 1; break;
	}
	if (input[1] != '\0') findseat(input + 1, ctx);
}

int bitcount(unsigned char byte) {
	int ret = 0;
	for (int i = 0 ; i < 8 ; ++i) {
		if (byte & (1 << i)) ++ret;
	}
	return ret;
}

int main(void) {
	const size_t buffer_l = 128;
	char buffer[buffer_l];

	unsigned char rows[128] = {0};

	FindseatCtx ctx;

	while (fgets(buffer, buffer_l, stdin)) {
		ctx = (const FindseatCtx){ 0, 127, 0, 7 };
		buffer[10] = '\0';
		findseat(buffer, &ctx);

		rows[ctx.front] |= 1 << ctx.left;

	}

	char seatbit;
	int seat;
	for (int i = 0 ; i < 128 ; ++i) {
		// our seat is the only empty one on it's row.
		if (bitcount(rows[i]) == 7) {
			seatbit = ~rows[i];
			for (seat = 0 ; (seatbit >> seat) != 1 ; ++seat);
			printf("my seat: %d, %d id: %d\n", i, seat, i * 8 + seat);
			break;
		}
	}

	return 0;
}
