/**
 * For this one, I went nuts and imlemented it in java.
**/
import java.io.*;

class ch2 {
	public static void main(String[] args) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		Player p_arr[] = {
			new Player(1,1),
			new Player(3,1),
			new Player(5,1),
			new Player(7,1),
			new Player(1,2)
		};
		PlayerList p = new PlayerList(p_arr);
		try {
			String line = br.readLine();
			Player.linelen = line.length();
			while ((line = br.readLine()) != null) {
				p.advance();
				char data[] = line.toCharArray();
				p.collide(data);
				p.print(data);
			}
		} catch (IOException ioe) {
			System.out.println(ioe);
		}
		System.out.print("Total (muliplied) collisions: ");
		System.out.println(p.total_collisions());
	}
}
