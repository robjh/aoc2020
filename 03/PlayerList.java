class PlayerList {
	private Player players[];
	public PlayerList(Player players[]) {
		this.players = players;
	}
	public void advance() {
		for (int i = 0 ; i < this.players.length ; ++i)
			this.players[i].advance();
	}
	public void collide(char data[]) {
		for (int i = 0 ; i < this.players.length ; ++i)
			this.players[i].collide(data);
	}
	public long total_collisions() {
		long running_total = this.players[0].total_collisions();
		for (int i = 1 ; i < this.players.length ; ++i)
			running_total *= this.players[i].total_collisions();
		return running_total;
	}
	public void print(char data[]) {
		// This function may produce output which appears incorrect
		// in the case of "down" being more than one.
		// But I'm adding this function for vanity anyhow and don't really care.
		for (int i = 0 ; i < this.players.length ; ++i) {
			int pos = this.players[i].position();
			switch (data[pos]) {
				case '#':
				case 'X':
					data[pos] = 'X';
					break;
				default:
					data[pos] = 'O';
					break;
			}
		}
		System.out.println(data);
	}
}
