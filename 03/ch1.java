/**
 * For this one, I went nuts and imlemented it in java.
**/
import java.io.*;

class ch1 {
	public static void main(String[] args) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		Player p = new Player(3,1);
		try {
			String line = br.readLine();
			Player.linelen = line.length();
			System.out.println(line);
			while ((line = br.readLine()) != null) {
				p.advance();
				char data[] = line.toCharArray();
				data[p.position()] = p.collide(data) ? 'X' : 'O';
				System.out.println(data);
			}
		} catch (IOException ioe) {
			System.out.println(ioe);
		}
		System.out.print("Total collisions: ");
		System.out.println(p.total_collisions());
	}
}
