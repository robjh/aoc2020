class Player {
	static public int linelen;
	private int right;
	private int down;
	private int x;
	private int y;
	private int collisions;
	public Player(int right, int down) {
		this.right = right;
		this.down = down;
		this.collisions = 0;
	}
	public void advance() {
		++this.y;
		if (this.y % this.down == 0) {
			this.x += this.right;
		}
	}
	public int position() {
		return this.x % Player.linelen;
	}
	public boolean collide(char data[]) {
		if (this.y % this.down == 0) {
			if (data[this.position()] == '#') {
				++this.collisions;
				return true;
			}
		}
		return false;
	}
	public int total_collisions() {
		return this.collisions;
	}
}
