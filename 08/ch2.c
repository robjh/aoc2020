#include <stdio.h>
#include <string.h>
#include <stdlib.h>

enum Opcode {
	END,
	NOP,
	ACC,
	JMP,
};

enum Error {
	NOERR,
	INFINATE_LOOP,
	UNKNOWN_OPCODE
};

typedef struct Instruction {
	enum Opcode opcode;
	int argument;
	int runcount;
} Instruction;

const char * opcodetostr(enum Opcode opcode) {
	switch (opcode) {
		case END: return "END";
		case NOP: return "NOP";
		case ACC: return "ACC";
		case JMP: return "JMP";
		default: break;
	}
	return "INVALID";
}

#define backtrace_l 1000

typedef struct ProgramCtx {
	int accumulator;
	int pc;
	enum Error error;
	int quiet;
	int counter;
	int backtrace[backtrace_l];
} ProgramCtx;

int run_program(Instruction * program, ProgramCtx * ctx) {
	int quiet = ctx->quiet;
	memset(ctx, 0, sizeof(ProgramCtx));

	while (program[ctx->pc].opcode != END) {
		ctx->backtrace[++ctx->counter] = ctx->pc;
		if (program[ctx->pc].runcount++) {
			printf("Error, infinate loop detected at %d\n", ctx->pc);
			ctx->error = INFINATE_LOOP;
			goto abort;
		}
		switch (program[ctx->pc].opcode) {
			case ACC:
				quiet || printf("%d ACC %d\n", ctx->pc, program[ctx->pc].argument);
				ctx->accumulator += program[ctx->pc].argument;
				++ctx->pc;
				break;
			case NOP:
				quiet || printf("%d NOP %d\n", ctx->pc, program[ctx->pc].argument);
				++ctx->pc;
				break;
			case JMP:
				quiet || printf("%d JMP %d\n", ctx->pc, program[ctx->pc].argument);
				ctx->pc += program[ctx->pc].argument;
				break;
			default:
				printf("Error, unknown Opcode: %d at %d\n", program[ctx->pc].opcode, ctx->pc);
				ctx->error = UNKNOWN_OPCODE;
				goto abort;
		}
	}
	abort:

	return ctx->accumulator;
}

int main(void) {
	const size_t buffer_l = 16;
	const size_t program_l = 650;
	char buffer[buffer_l];
	Instruction program[program_l];
	Instruction program_cpy[program_l];

	unsigned int sum = 0;
	unsigned int q_reg = 0;

	for (int i = 0 ; i < program_l && fgets(buffer, buffer_l, stdin) ; ++i) {
		if(buffer[0] == '\n') continue; // empty line
		if      (memcmp(buffer, "nop", 3) == 0) program[i].opcode = NOP;
		else if (memcmp(buffer, "acc", 3) == 0) program[i].opcode = ACC;
		else if (memcmp(buffer, "jmp", 3) == 0) program[i].opcode = JMP;
		program[i].argument = atoi(buffer+4);
		program[i].runcount = 0;
	}

	ProgramCtx rawctx = { 0 };
	memcpy(program_cpy, program, sizeof(Instruction) * program_l);

	int acc = run_program(program_cpy, &rawctx);

	for (int i = rawctx.counter-1 ; i ; --i) {
		if (
			program[rawctx.backtrace[i]].opcode != NOP &&
			program[rawctx.backtrace[i]].opcode != JMP
		) {
			continue;
		}

		memcpy(program_cpy, program, sizeof(Instruction) * program_l);
		program_cpy[rawctx.backtrace[i]].opcode = (
			program[rawctx.backtrace[i]].opcode == NOP ? JMP : NOP
		);

		ProgramCtx cpyctx = { 0 };
		cpyctx.quiet = 1;
		acc = run_program(program_cpy, &cpyctx);
		if (cpyctx.error == NOERR) {
			printf(
				"no error!\n"
				"acc was: %d\n",
				acc
			);
			break;
		}
	}


	return 0;
}
