#include <stdio.h>
#include <string.h>
#include <stdlib.h>

enum Opcode {
	END,
	NOP,
	ACC,
	JMP
};

typedef struct Instruction {
	enum Opcode opcode;
	int argument;
	int runcount;
} Instruction;

int run_program(Instruction * program) {
	int accumulator = 0;
	int pc = 0;

	while (program[pc].opcode != END) {
		if (program[pc].runcount++) {
			printf("Error, infinate loop detected at %d\n", pc);
			goto abort;
		}
		switch (program[pc].opcode) {
			case ACC:
				printf("ACC %d\n", program[pc].argument);
				accumulator += program[pc].argument;
				++pc;
				break;
			case NOP:
				printf("NOP %d\n", program[pc].argument);
				++pc;
				break;
			case JMP:
				printf("JMP %d\n", program[pc].argument);
				pc += program[pc].argument;
				break;
			default:
				printf("Error, unknown Opcode: %d at %d\n", program[pc].opcode, pc);
				goto abort;
		}
	}
	abort:

	return accumulator;
}

int main(void) {
	const size_t buffer_l = 16;
	const size_t program_l = 650;
	char buffer[buffer_l];
	Instruction program[program_l];

	unsigned int sum = 0;
	unsigned int q_reg = 0;

	for (int i = 0 ; i < program_l && fgets(buffer, buffer_l, stdin) ; ++i) {
		if(buffer[0] == '\n') continue; // empty line
		if      (memcmp(buffer, "nop", 3) == 0) program[i].opcode = NOP;
		else if (memcmp(buffer, "acc", 3) == 0) program[i].opcode = ACC;
		else if (memcmp(buffer, "jmp", 3) == 0) program[i].opcode = JMP;
		program[i].argument = atoi(buffer+4);
		program[i].runcount = 0;
	}

	int acc = run_program(program);
	printf("acc was: %d\n", acc);

	return 0;
}
