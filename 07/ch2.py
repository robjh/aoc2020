#!/usr/bin/env python
import sys
import re

class Rule:
	def __init__(self, count, link):
		self.count = count
		self.link = link

class Bag:
	def __init__(self, name):
		self.name = name
		self.parents = []
		self.rules = []
		self.flag = 0

	def recursive_count_bags(self):
		total = 0;
		for rule in self.rules:
			total += (1 + rule.link.recursive_count_bags()) * rule.count
		return total

def main():
	rulebook = {};

	for line in sys.stdin:
		match = re.search("^([a-z ]+) bags contain ", line)
		if (match):
			parentname = match.group(1)
			if not (parentname in rulebook):
				rulebook[parentname] = Bag(parentname)
		else:
			continue

		match = re.findall("(\d) ([a-z ]+) bag(?:s)?(?:, |\.)", line)
		for child in match:
			if not (child[1] in rulebook):
				rulebook[child[1]] = Bag(child[1])
			rulebook[parentname].rules.append(Rule(int(child[0]), rulebook[child[1]]))
			rulebook[child[1]].parents.append(rulebook[parentname])

	print(rulebook["shiny gold"].recursive_count_bags())

if __name__ == "__main__":
	main()
