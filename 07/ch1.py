#!/usr/bin/env python
import sys
import re

class Rule:
	def __init__(self, count, link):
		self.count = count
		self.link = link

class Bag:
	def __init__(self, name):
		self.name = name
		self.parents = []
		self.rules = []
		self.flag = 0

	def recursive_mark_parents(self):
		for p in self.parents:
			p.flag = 1
			p.recursive_mark_parents()

def main():
	rulebook = {};

	for line in sys.stdin:
		match = re.search("^([a-z ]+) bags contain ", line)
		if (match):
			parentname = match.group(1)
			if not (parentname in rulebook):
				rulebook[parentname] = Bag(parentname)
		else:
			continue

		match = re.findall("(\d) ([a-z ]+) bag(?:s)?(?:, |\.)", line)
		for child in match:
			if not (child[1] in rulebook):
				rulebook[child[1]] = Bag(child[1])
			rulebook[parentname].rules.append(Rule(child[0], rulebook[child[1]]))
			rulebook[child[1]].parents.append(rulebook[parentname])

	rulebook["shiny gold"].recursive_mark_parents()
	total_valid_parents = 0;
	for bag in rulebook:
		if rulebook[bag].flag:
			print(bag)
			total_valid_parents += 1
	print(total_valid_parents)

if __name__ == "__main__":
	main()
