#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Rule {
	int first;
	int second;
	char letter;
	const char * password;
} Rule;

void extract_rule(char * string, Rule * rule) {
	/* our input looks like this:
	   (\d{1,2})-(\d{1,2}) ([a-z]): ([a-z]+)
	*/

	rule->first = atoi(string);

	/* look for a dash */
	while(*string != '\0' && *string != '-') ++string;
	++string;
	rule->second = atoi(string);

	/* look for a space */
	while(*string != '\0' && *string != ' ') ++string;
	++string;
	rule->letter = string[0];

	/* look for another space */
	while(*string != '\0' && *string != ' ') ++string;
	rule->password = ++string;

	/* look for the new line char */
	while(*string != '\0' && *string != '\n') ++string;
	string[0] = '\0';
}

int validate(const Rule * rule) {
	return (rule->password[rule->first - 1] == rule->letter
	    || rule->password[rule->second - 1] == rule->letter)
	    && rule->password[rule->first - 1] != rule->password[rule->second - 1];
}

int main(void) {
	const size_t buffer_l = 128;
	char buffer[buffer_l];
	Rule rule;
	int line = 0;
	int valid_password_count = 0;
	int valid;
	while (fgets(buffer, buffer_l, stdin)) {
		extract_rule(buffer, &rule);
		if (valid = validate(&rule)) ++valid_password_count;
		printf("%d) %d - %d '%c' : \"%s\" %s\n", ++line, rule.first, rule.second, rule.letter, rule.password, valid ? "✔️" : "❌");
	}
	printf("valid count: %d\n", valid_password_count);
	return 0;
}
