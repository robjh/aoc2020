#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Rule {
	int min;
	int max;
	char letter;
	const char * password;
} Rule;

void extract_rule(char * string, Rule * rule) {
	rule->min = atoi(string);

	/* look for a dash */
	while(*string != '\0' && *string != '-') ++string;
	++string;
	rule->max = atoi(string);

	/* look for a space */
	while(*string != '\0' && *string != ' ') ++string;
	++string;
	rule->letter = string[0];

	/* look for another space */
	while(*string != '\0' && *string != ' ') ++string;
	rule->password = ++string;

	/* look for the new line char */
	while(*string != '\0' && *string != '\n') ++string;
	string[0] = '\0';
}

int validate(const Rule * rule) {
	int count = 0;
	for (const char * i = rule->password; *i != '\0' ; ++i) {
		if (*i == rule->letter) ++count;
	}
	return count >= rule->min && count <= rule->max;
}

int main(void) {
	const size_t buffer_l = 128;
	char buffer[buffer_l];
	Rule rule;
	int valid_password_count = 0;
	while (fgets(buffer, buffer_l, stdin)) {
		extract_rule(buffer, &rule);
		if (validate(&rule)) ++valid_password_count;
		printf("%d - %d '%c' : \"%s\" %d\n", rule.min, rule.max, rule.letter, rule.password, validate(&rule));
	}
	printf("valid count: %d\n", valid_password_count);
	return 0;
}
