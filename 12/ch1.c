#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct Vector {
	int x;
	int y;
} Vector;

int manhattan(const Vector * v) {
	return (v->x >= 0 ? v->x : -v->x) + (v->y >= 0 ? v->y : -v->y);
}

// Assumes all changes of baring happen in 90 degree increments.
int main(const int argc, const char * argv[]) {
	const size_t buffer_l = 16;
	char buffer[buffer_l];
	int value;

	const int baring_l = 4;
	const Vector baring[4] = {{ 1, 0 }, { 0, -1 }, { -1, 0 }, { 0, 1 }}; // E, S, W, N
	int baring_i = 0;
	Vector pos = { 0, 0 };

	while (fgets(buffer, buffer_l, stdin)) {
		if(buffer[0] == '\n') continue; // empty line
		value = atoi(buffer+1);
		switch (buffer[0]) {
			case 'F':
				pos.x += baring[baring_i].x * value;
				pos.y += baring[baring_i].y * value;
				break;
			case 'L': 
				baring_i = (baring_l + baring_i - value / 90) % baring_l;
				break;
			case 'R': 
				baring_i = (baring_l + baring_i + value / 90) % baring_l;
				break;

			// cardinals
			case 'E':
				pos.x += value;
				break;
			case 'W':
				pos.x -= value;
				break;
			case 'N':
				pos.y += value;
				break;
			case 'S':
				pos.y -= value;
				break;
				
		}
//		printf("%c %d (%d, %d)\n", buffer[0], value,  pos.x, pos.y);
	}

	printf("%d\n", manhattan(&pos));

	return 0;
}
