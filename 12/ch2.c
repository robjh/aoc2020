#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct Vector {
	int x;
	int y;
} Vector;

int manhattan(const Vector * v) {
	return (v->x >= 0 ? v->x : -v->x) + (v->y >= 0 ? v->y : -v->y);
}

typedef int Matrix[4];

void matrix_multiply(const Matrix m, const Vector * v, Vector * result) {
	result->x = v->x * m[0] + v->y * m[1];
	result->y = v->x * m[2] + v->y * m[3];
}

// Assumes all changes of baring happen in 90 degree increments.
int main(const int argc, const char * argv[]) {
	const size_t buffer_l = 16;
	char buffer[buffer_l];
	int value;

	const Matrix matrix[] = {{
		1, 0,  // identity
		0, 1
	}, {
		0, 1, // 90 degrees
		-1, 0
	}, {
		-1, 0, // 180 degrees
		0, -1
	}, {
		0, -1, // 270 degrees
		1, 0
	}};

	Vector pos = { 0, 0 };
	Vector waymark = { 10, 1 };
	Vector result;

	while (fgets(buffer, buffer_l, stdin)) {
		if(buffer[0] == '\n') continue; // empty line
		value = atoi(buffer+1);
		switch (buffer[0]) {
			case 'F':
				pos.x += waymark.x * value;
				pos.y += waymark.y * value;
				break;
			case 'L':
				value = value/-90 + 4;
				matrix_multiply(matrix[value], &waymark, &result);
				waymark = result;
				break;
			case 'R':
				value /= 90;
				matrix_multiply(matrix[value], &waymark, &result);
				waymark = result;
				break;

			// cardinals
			case 'E':
				waymark.x += value;
				break;
			case 'W':
				waymark.x -= value;
				break;
			case 'N':
				waymark.y += value;
				break;
			case 'S':
				waymark.y -= value;
				break;

		}
//		printf("%c %d (%d, %d) (%d, %d)\n", buffer[0], value,  pos.x, pos.y,  waymark.x, waymark.y);
	}

	printf("%d\n", manhattan(&pos));

	return 0;
}
