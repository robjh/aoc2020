#include <stdio.h>
#include <string.h>
#include <stdint.h>

#define SEAT_F         0x80
#define OCCUPIED_F     0x01
#define WAS_OCCUPIED_F 0x02
#define SEAT_MASK      0x03

#define SEAT_L 10000

typedef struct SeatCtx {
	size_t col;
	size_t row;
	uint8_t data[SEAT_L];
} SeatCtx;
#define POS(ctx_p, x, y) (ctx_p->data[y*ctx->col + x])

void print_seatctx(const SeatCtx * ctx) {
	for (size_t y = 0 ; y < ctx->row ; ++y) {
		for (size_t x = 0 ; x < ctx->col ; ++x) {
			printf("%c", POS(ctx, x, y) & SEAT_F ? (
				POS(ctx, x, y) & OCCUPIED_F ? '#' : 'L'
			) : '.');
		}
		printf("\n");
	}
}

void slide_up(SeatCtx * ctx) {
	for (size_t y = 0 ; y < ctx->row ; ++y) {
		for (size_t x = 0 ; x < ctx->col ; ++x) {
			if (POS(ctx,x,y) & SEAT_F) {
				if (POS(ctx,x,y) & OCCUPIED_F) {
					POS(ctx,x,y) = (POS(ctx,x,y) << 1) | SEAT_F | OCCUPIED_F;
				} else {
					POS(ctx,x,y) = (POS(ctx,x,y) << 1) | SEAT_F;
				}
			}
		}
	}
}

int inbounds(const SeatCtx * ctx, int x, int y) {
	return x >= 0 && x < ctx->col && y >= 0 && y < ctx->row;
}

int adjacent_occupied(SeatCtx * ctx, size_t posx, size_t posy) {
	int ret = 0;
	const uint8_t mask = SEAT_F | WAS_OCCUPIED_F;
	for (int yi = -1 ; yi <= 1 ; ++yi) for (int xi = -1 ; xi <= 1 ; ++xi) {
		if (!xi && !yi) continue;
		for (int s = 1 ; inbounds(ctx, posx+xi*s, posy+yi*s) ; ++s) {
			int x = posx + xi * s;
			int y = posy + yi * s;
			if (POS(ctx, x, y) & SEAT_F) {
				if ((POS(ctx, x, y) & mask) == mask) ++ret;
				break;
			}
		}
	}
	return ret;
}

void apply_rules(SeatCtx * ctx) {
	slide_up(ctx);
	for (size_t y = 0 ; y < ctx->row ; ++y) {
		for (size_t x = 0 ; x < ctx->col ; ++x) {
			if (POS(ctx,x,y) & SEAT_F) {
				if ((POS(ctx,x,y) & WAS_OCCUPIED_F) == 0) {
//					printf("L");
					if (adjacent_occupied(ctx,x,y) == 0) {
						POS(ctx,x,y) |= OCCUPIED_F;
					}
				}
				if ((POS(ctx,x,y) & WAS_OCCUPIED_F) == WAS_OCCUPIED_F) {
//					printf("%d", adjacent_occupied(ctx,x,y));
					if (adjacent_occupied(ctx,x,y) >= 5) {
						POS(ctx,x,y) &= ~OCCUPIED_F;
					}
				}
			}
//			else printf(".");
		}
//		printf("\n");
	}
}

int count_changes(const SeatCtx * ctx) {
	uint8_t mask = 0x03;
	int changes = 0;
	for (size_t y = 0 ; y < ctx->row ; ++y) {
		for (size_t x = 0 ; x < ctx->col ; ++x) {
			switch (POS(ctx,x,y) & mask) {
				case 1:
				case 2:
					++changes;
				default:
					break;
			}
		}
	}
	return changes;
}

int count_occupied(const SeatCtx * ctx) {
	int occupied = 0;
	for (size_t y = 0 ; y < ctx->row ; ++y) {
		for (size_t x = 0 ; x < ctx->col ; ++x) {
			if (POS(ctx,x,y) & OCCUPIED_F) ++occupied;
		}
	}
	return occupied;
}

int main(const int argc, const char * argv[]) {
	const size_t buffer_l = 100;
	char buffer[buffer_l];
	SeatCtx ctx = { 0 };

	while (fgets(buffer, buffer_l, stdin)) {
		if(buffer[0] == '\n') continue; // empty line
		for (size_t i = 0; buffer[i] != '\n' ; ++i) {
			if (!ctx.col) {
				ctx.col = strlen(buffer) - 1;
			}
			switch (buffer[i]) {
				case 'L':
					ctx.data[ctx.row * ctx.col + i] = SEAT_F;
					break;
				case '#':
					ctx.data[ctx.row * ctx.col + i] = SEAT_F | OCCUPIED_F;
					break;
				case '.':
					// the floor, not a seat and not occupied.
					ctx.data[ctx.row * ctx.col + i] = 0;
					break;
				default:
					printf("bad input on line %d,%d: '%c'.", ctx.row+1, i+1, buffer[i]);
					ctx.data[ctx.row * ctx.col + i] = 0;
					break;
			}
		}
		++ctx.row;
	}

	int changes;
	do {
		apply_rules(&ctx);
		changes = count_changes(&ctx);
	} while(changes);

	print_seatctx(&ctx);
	printf("occupied: %d\n", count_occupied(&ctx));

	return 0;
}
